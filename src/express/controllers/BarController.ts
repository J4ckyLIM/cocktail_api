import { Router, Response, Request } from "express";
import { BarCreateDTO } from "../../definitions/dto/bar/BarCreateDTO";
import { MongooseUtils } from "../../mongoose";
import { BaseController } from "./BaseController";

export class BarController extends BaseController {

  async create(req: Request, res: Response) {
    const dto = await BarController.createAndValidateDTO(BarCreateDTO, req.body, res);
    if(!dto) return;
    try {
        const bar = await this.serviceRegistry.barService.create(dto);
        res.status(201).json(bar);
    } catch (err) {
        MongooseUtils.isDuplicateKeyError(err) ? res.status(409).end() : res.status(500).end();
    }
  }

  buildRoutes(): Router {
    const router = Router();
    router.post('/new', this.create.bind(this));
    return router;
  }
}