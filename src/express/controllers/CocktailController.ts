import { Router, Response, Request } from "express";
import { CocktailCreateDTO } from "../../definitions";
import { MongooseUtils } from "../../mongoose";
import { BaseController } from "./BaseController";

export class CocktailController extends BaseController {

  async create(req: Request, res: Response) {
    const dto = await CocktailController.createAndValidateDTO(CocktailCreateDTO, req.body, res);
    if(!dto) return;
    try {
        const cocktail = await this.serviceRegistry.cocktailService.create(dto);
        res.status(201).json(cocktail);
    } catch (err) {
        MongooseUtils.isDuplicateKeyError(err) ? res.status(409).end() : res.status(500).end();
    }
  }

  async searchById(req: Request, res: Response) {
    const { id } = req.params;
    if(!id) {
      res.status(404).end();
      return;
    }

    const cocktail = await this.serviceRegistry.cocktailService.getById(id);
    if(!cocktail) {
      res.status(404).end();
      return;
    }

    res.status(201).json(cocktail);
  }

  async deleteById(req: Request, res: Response) {
    const { id } = req.params;
    if(!id) {
      res.status(404).end();
      return;
    }

    const deletedCocktail = await this.serviceRegistry.cocktailService.deleteById(id);
    if(!deletedCocktail) {
      res.status(404).end();
      return;
    }

    res.status(201).json({ message: 'The cocktail has been successfully deleted'});
  }

  buildRoutes(): Router {
    const router = Router();
    router.post('/new', this.create.bind(this));
    router.get('/:id', this.searchById.bind(this));
    router.delete('/:id', this.deleteById.bind(this));
    return router;
  }
}