import { Response, Router } from "express";
import { ServiceRegistry } from "../../mongoose";
import { plainToInstance } from "class-transformer";
import { validate } from "class-validator";

export abstract class BaseController {

    protected serviceRegistry: ServiceRegistry;

    constructor(serviceRegistry: ServiceRegistry) {
      this.serviceRegistry = serviceRegistry;
    }

    static async createAndValidateDTO<DTO extends object>(constructor: new (...args: any[]) => DTO, raw: any, res: Response): Promise<DTO | undefined> {
      const dto = plainToInstance(constructor, raw);
      const errors = await validate(dto);
      if(errors.length > 0) {
        const obj = errors.reduce<{ [key: string]: any }>((acc, err) => {
          acc[err.property] = err.constraints;
          return acc;
        }, {})
        res.status(400).json(obj);
        return;
      }
      return dto;
    }

    abstract buildRoutes(): Router;
}