import * as express from "express";

import { ServiceRegistry } from "../mongoose";
import { CocktailController, BarController } from "./controllers";

export const startServer = async (serviceRegistry: ServiceRegistry) => {
    const app = express();
    app.use(express.json()); // Parse http body to JSON

    const cocktailController = new CocktailController(serviceRegistry);
    const barController = new BarController(serviceRegistry);
    app.use('/cocktail', cocktailController.buildRoutes()); // save all the routes for the given controller
    app.use('/bar', barController.buildRoutes());

    app.listen(process.env.PORT, () => {
        console.log(`Server listening on port ${process.env.PORT}...`)
    })
}