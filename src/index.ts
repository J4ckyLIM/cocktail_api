import 'reflect-metadata'; // MANDATORY for annotations
import { config } from "dotenv"
import { startServer } from "./express";
import { MongooseUtils, ServiceRegistry } from "./mongoose";

config()

const main = async () => {
    const connection = await MongooseUtils.connect();
    const serviceRegistry = new ServiceRegistry(connection);
    startServer(serviceRegistry);
}

main().catch(console.error);