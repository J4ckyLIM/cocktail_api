import { TransformFnParams } from "class-transformer";

/**
 * This transform a string separated by commas ',' to '|'
 * @param params String representation
 * @returns the formatted input
 * @example "one, two, three" becomes "one | two | three"
 */
export const textTransformerHandler = (params: TransformFnParams): any => {
  if(typeof params.value === 'string' && params.value.length >= 10) {
      return params.value.replace(/,/g, '|')
  }

  return params.value
}