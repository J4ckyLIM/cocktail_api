import { Schema, SchemaTypes } from "mongoose";
import { ICocktail } from "../../definitions";

export const CocktailSchema = new Schema({
  name: {
    type: SchemaTypes.String,
    required: true,
    minlength: 1,
    unique: true
  },
  price: {
    type: SchemaTypes.Number,
    required: true,
  },
  alcohol: {
    type: SchemaTypes.Number,
    required: true,
  },
  ingredients: {
    type: SchemaTypes.String,
    required: true,
  },
  description: {
    type: SchemaTypes.String,
    required: true,
  }
}, {
  timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
  },
  versionKey: false,
  collection: 'cocktails'
})

export type ICocktailDocument = ICocktail & Document;