import { Schema, SchemaTypes } from "mongoose";
import { IBar } from "../../definitions";

export const BarSchema = new Schema({
  location: {
    lat: Number,
    lon: Number,
  },
  address: {
    type: SchemaTypes.String,
    require: true
  },
  cocktails: {
    type: [Schema.Types.ObjectId],
    ref: 'Cocktail',
  },
}, {
  timestamps: {
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
  },
  versionKey: false,
  collection: 'bars'
})

export type IBarDocument = IBar & Document;