import { Mongoose } from "mongoose";
import { BarService } from "./BarService";
import { CocktailService } from "./CocktailService";

export class ServiceRegistry {

    public readonly cocktailService: CocktailService;
    public readonly barService: BarService;

    constructor(connection: Mongoose) {
        this.cocktailService = new CocktailService(connection);
        this.barService = new BarService(connection);
    }
}