import { Mongoose } from "mongoose";

export abstract class BaseService {
  
  protected connection: Mongoose;

  constructor(connection: Mongoose) {
    this.connection = connection;
  }
}