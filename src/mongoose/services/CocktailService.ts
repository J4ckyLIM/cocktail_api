import { Model, Mongoose } from "mongoose";
import { CocktailCreateDTO } from "../../definitions";
import { CocktailSchema, ICocktailDocument } from "../schemas";
import { BaseService } from "./BaseService";

export class CocktailService extends BaseService {
  protected model: Model<ICocktailDocument>;
  
  constructor(connection: Mongoose) {
    super(connection);
    this.model = connection.model<ICocktailDocument>('Cocktail', CocktailSchema);
  }

  public create(dto: CocktailCreateDTO): Promise<ICocktailDocument> {
    return this.model.create(dto);
  }

  public getById(id: any): Promise<ICocktailDocument | null> {
    return this.model.findById(id).exec();
  }

  public deleteById(id: any): Promise<ICocktailDocument | null> {
    return this.model.findByIdAndDelete(id).exec();
  }
}