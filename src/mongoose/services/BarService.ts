import { Model, Mongoose } from "mongoose";
import { BarCreateDTO } from "../../definitions/dto/bar/BarCreateDTO";
import { BarSchema, IBarDocument } from "../schemas";
import { BaseService } from "./BaseService";

export class BarService extends BaseService {
  protected model: Model<IBarDocument>;

  constructor(connection: Mongoose) {
    super(connection);
    this.model = connection.model<IBarDocument>('Bar', BarSchema);
  }

  public create(dto: BarCreateDTO): Promise<IBarDocument> {
    return this.model.create(dto);
  }
}