import { Expose, Transform } from "class-transformer";
import { IsNumber, IsString, MinLength } from "class-validator";
import { textTransformerHandler } from "../../../utils";

export class CocktailCreateDTO {
  
  @MinLength(1)
  @IsString()
  @Expose()
  name: string;

  @IsNumber()
  @Expose()
  price: number;

  @IsNumber()
  @Expose()
  alcohol: number;

  @MinLength(4)
  @IsString()
  @Expose()
  @Transform(textTransformerHandler)
  ingredients: string;

  @MinLength(10)
  @IsString()
  @Expose()
  description: string;
}