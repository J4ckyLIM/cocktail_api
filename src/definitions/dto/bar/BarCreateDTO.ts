import { Expose, Transform, Type } from "class-transformer";
import { IsArray, IsNotEmptyObject, IsNumber, IsObject, IsString, MinLength } from "class-validator";
import { textTransformerHandler } from "../../../utils";
import { ILocation } from "../../models";

export class LocationDTO {

  @IsNumber()
  @Expose()
  lat: number;

  @IsNumber()
  @Expose()
  lon: number;
}

export class BarCreateDTO {
  
  @IsObject()
  @IsNotEmptyObject()
  @Type(() => LocationDTO)
  @Expose()
  location: ILocation;

  @IsString()
  @Expose()
  address: string;

  @IsArray()
  @Expose()
  cocktails: string[];
}