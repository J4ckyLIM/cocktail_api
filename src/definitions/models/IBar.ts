import { ICocktail } from "./ICocktail";

export interface ILocation {
  lat: number;
  lon: number;
}

export interface IBar {
  _id: any;
  location: ILocation;
  address: string;
  cocktails: ICocktail[];
}