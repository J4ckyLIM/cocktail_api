export interface ICocktail {
  _id: any;
  name: string;
  price: number;
  alcohol: number;
  ingredients: string;
  description: string;
}